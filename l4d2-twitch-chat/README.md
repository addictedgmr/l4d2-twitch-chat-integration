Instructions for use:

Currently working and tested on Windows 10 in Single Player mode.  I'll be working to see if I can get it working in multiplayer.  This is a pure .NET Core app so it might work on linux/MacOs

L4D2 Prerequisites:
This tool requires that the Admin System mod is installed.  It can be found here: https://steamcommunity.com/sharedfiles/filedetails/?id=214630948

L4D2 Setup:
The console app runs in the background and emulates keyboard presses based on Twitch chat messages.  For this to work properly you will need to ensure that the bindings in the <a href="https://gitlab.com/addictedgmr/l4d2-twitch-chat-integration/-/blob/master/l4d2-twitch-chat/l4d2%20bindings.txt">bindings file</a> have been added by:
1) Enabling the L4D2 console
2) Ensuring none of the following keys are bound to anything in game: Y, U, I, G, H, J, K, L, V, B, N, M, 0, 9
3) Running the bind commands one by one in the L4D2 console

At this point you can load up a game and test to make sure that the keys are bound to the different functions like setting everyone on fire or throwing a molotov where your crosshairs are.

Setup:
1) Download the latest release and extract the folder
2) Setup L4D2 as outlined in the L4D2 Setup section
3) Start a Single Player game in L4D2
4) Start the console app by running or debugging it
5) Enter your username and oauth key into the console when prompted
6) Enjoy your chat messing with your game as much as possible
...
Profit