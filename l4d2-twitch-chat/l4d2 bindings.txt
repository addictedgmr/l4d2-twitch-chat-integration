bindings:
bind "Y" "scripted_user_func drop_spit";
bind "U" "scripted_user_func drop_fire";
bind "I" "scripted_user_func infinite_ammo";
bind "G" "scripted_user_func vomit,all";
bind "H" "scripted_user_func ignite,all";
bind "J" "scripted_user_func extinguish,all";
bind "K" "scripted_user_func god,all";
bind "L" "scripted_user_func ammo,all,50";
bind "V" "scripted_user_func upgrade_add,all,incendiary_ammo";
bind "B" "scripted_user_func upgrade_add,all,explosive_ammo";
bind "N" "scripted_user_func zombie,riot,5";
bind "M" "scripted_user_func zombie,common_male_tshirt_cargos,10";
bind "0" "scripted_user_func give,first_aid_kit";
bind "9" "scripted_user_func give,pain_pills";